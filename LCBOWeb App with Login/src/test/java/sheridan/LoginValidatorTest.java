package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;


public class LoginValidatorTest {


	
	@Test
	public final void testHasAlphaAndNumbersOnlyRegular() {
		String loginName="anuj123";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testHasAlphaAndNumbersOnlyException() {
		String loginName= "";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertFalse("The emai provided is not valid ",result);
	}

	@Test
	public final void testHasAlphaAndNumbersOnlyBoundaryIn() {
		String loginName="anujch";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testHasAlphaAndNumbersOnlyBoundaryOut() {
		String loginName="anujc&";
		boolean result=LoginValidator.hasAlphaAndNumbersOnly(loginName);
		assertFalse("The emai provided in not valid ",result);
	}

	
	
	
	//---------------------------------------------------------
	
	
	@Test
	public final void testStartsWithAlphabetRegular() {
		String loginName="anujchoudhary";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testStartsWithAlphabetException() {
		String loginName= "@%#$@#$@";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertFalse("The emai provided is not valid ",result);
	}

	@Test
	public final void testStartsWithAlphabetBoundaryIn() {
		String loginName="a34123";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertTrue("The email provided is valid ",result);
	}

	@Test
	public final void testStartsWithAlphabetBoundaryOut() {
		String loginName="5choua";
		boolean result=LoginValidator.startsWithAlphabet(loginName);
		assertFalse("The emai provided in not valid ",result);
	}

	
	
	
}
