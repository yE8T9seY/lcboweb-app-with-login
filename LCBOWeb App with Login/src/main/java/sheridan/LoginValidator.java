package sheridan;

public class LoginValidator {

	
	
	//
	//Login name must have alpha characters and numbers only, but should not start with a number
	//Login name must have at least 6 alphanumeric characters.
	
	
	public static boolean hasAlphaAndNumbersOnly( String loginName ) {
		if(loginName.length()<6)
			return false;
		
		for(int i = 0; i <loginName.length();i++)
		{
			if(Character.isAlphabetic(loginName.charAt(i)) || Character.isDigit(loginName.charAt(i)))
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
	
	public static boolean startsWithAlphabet( String loginName ) {
		
		
		if(Character.isDigit(loginName.charAt(0)))
		{
			return false;
		}
		
		
		for(int i = 0; i <loginName.length();i++)
		{
			if(Character.isAlphabetic(loginName.charAt(i)) || Character.isDigit(loginName.charAt(i)))
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}
}
